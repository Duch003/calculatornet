# README #

A simple calculator written to summarize my knowledge at the time after finishing the book "C#. Practical course" by Marcin Lis.

### What is this repository for? ###

* .NET Framwework 4.5.2, WinForms
* 1.0

### How do I get set up? ###

* Prerequisites: installed .NET Framework 4.5.2 Runtime, installed Visual Studio or Visual Studio Code.
* Download as ZIP or clone repository to your local machine
* Open Calculator.sln
* Run and enjoy!